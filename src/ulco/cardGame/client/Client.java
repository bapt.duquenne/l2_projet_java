package ulco.cardGame.client;

import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.CardPlayer;
import ulco.cardGame.server.SocketServer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {


    public static void main(String[] args) {

        // Current use full variables
        try {

            // Create a connection to the server socket on the server application
            InetAddress host = InetAddress.getLocalHost();
            Socket socket;
            Player me = new CardPlayer("Uninitialized");
            Game game ;
            Object answer;
            String username;

            Scanner scanner = new Scanner(System.in);

            System.out.println("Please select your username");
            username = scanner.nextLine();

            socket = new Socket(host.getHostName(), SocketServer.PORT);

            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(username);

            do {
                // Read and display the response message sent by server application
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                answer = ois.readObject();

                // depending of object type, we can manage its data
                if (answer instanceof String)
                    if(((String) answer).startsWith("[ "+username+" ] C'est ton tour ! \n")){
                        System.out.println(answer.toString());
                        me.play(socket);
                    }else {
                        System.out.println(answer.toString());
                    }
                if (answer instanceof Game){
                    game = (Game) answer;
                    ((Game)answer).displayState();
                }
                if(answer instanceof Player){
                    me =(Player) answer;
                }



            } while (!answer.equals("END"));

            // close the socket instance connection
            socket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
